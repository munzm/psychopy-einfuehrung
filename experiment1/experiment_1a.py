#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Die ersten beiden Linien sind Kommentare. Die erste Selegiert den sich
# zuvorderst auf dem Pfad befindlichen Interpreter. Die zweite verhindert
# Probleme mit Umlauten etc."

# core: Basisfunktionen; visual: Stimulipraesentation; event: Vp-Input
from psychopy import core, visual, event

mywindow = visual.Window(fullscr = False,
                         monitor = 'uniHp')

begrtext = """Liebe Versuchsperson,

Vielen Dank, dass Sie bei unserem Experiment mitmachen!

Wir hoffen, es bereitet Ihnen viel Spass!"""

mytext = visual.TextStim(win        = mywindow,
                         text       = begrtext,
                         alignVert  = 'top',
                         alignHoriz = 'left',
                         pos        = (-.8, 0.8),
                         units      = 'norm',
                         wrapWidth  = 1.6)

# Zeichnen des Stimulus in das Fenster.
mytext.draw()
# Zeichnen des Stimulus auf dem Monitor.
mywindow.flip()

# Die Versuchsperson soll Zeit haben, die Nachricht zu lesen
core.wait(5.0)

# Zeige einen leeren Schirm fuer 2 Sekunden.
mywindow.clearBuffer()
mywindow.flip()
core.wait(2)

# Wir schliessen das Fenster und beenden Psychopy.
mywindow.close()
core.quit()
