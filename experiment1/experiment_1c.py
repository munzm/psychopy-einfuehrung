#!/usr/bin/env python
# -*- coding: utf-8 -*-
from psychopy import core, visual, logging

letters = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]
LETTERS = [letter.upper() for letter in letters]

# Wieviele Frames lang sollen Stimuli gezeigt werden?
nframes = 10

# Wie lange sollen Stimuli gezeigt werden? (Clock Methode)
pres_duration = nframes * (1/60.0)  # Mein Monitor hat 60 Herz

mywin = visual.Window(fullscr = True)
mystim = visual.TextStim(mywin)

# Wir wollen untersuchen, ob Frames gedroppt werden.
mywin.setRecordFrameIntervals(True)
mywin._refreshThreshold=1/60.0+0.004 # Mein Monitor hat 60 Hz und wir erlauben
                                     # 4ms Toleranz.
logging.console.setLevel(logging.WARNING) # Logge Warnings im Terminal

## Clock Methode.
core.wait(1)
pres_times_clock = [] # Speichert Dauer jeder Stimuluspraesentation.

for l in LETTERS:
    mystim.setText(l)
    clock = core.Clock()
    while clock.getTime() < pres_duration:
        mystim.draw()
        mywin.flip()
    pres_times_clock.append(clock.getTime())

mywin.clearBuffer()
mywin.flip()

# Frame Methode
pres_times_frames = [] # Speichert Dauer jeder Stimuluspraesentation.

for l in LETTERS:
    mystim.setText(l)
    my_clock = core.Clock()
    for frame in range(nframes): # Genau nframes frames Mal.
        # Man unkommentiere die beiden folgenden Linien,
        # um zu sehen, wie eine Warnung  aufgrund eines
        # gedroppten Frames aussieht.
        # if l == "A":
        #     core.wait(1)
        mystim.draw()
        mywin.flip()
    pres_times_frames.append(my_clock.getTime())

mywin.close()

# Zusammenfassung des Timinigs.
def mean(lst):
    sum = 0
    for i in lst:
        sum += i
    return(sum / len(lst))

mean_pres_durations = (mean(pres_times_clock), mean(pres_times_frames))

print "mittlere Dauer clock-Methode: %.4f" % mean_pres_durations[0]
print "Mittlere Dauer frame-Methode: %.4f" % mean_pres_durations[1]
