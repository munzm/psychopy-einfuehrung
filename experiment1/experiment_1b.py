#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Die ersten beiden Linien sind Kommentare. Die erste Selegiert den sich
# zuvorderst auf dem Pfad befindlichen Interpreter. Die zweite verhindert
# Probleme mit Umlauten etc."

# core: Basisfunktionen; visual: Stimulipraesentation; event: Vp-Input
from psychopy import core, visual, event

mywindow = visual.Window(fullscr = False,
                         monitor = 'uniHp')

begrtext = """Liebe Versuchsperson,

Vielen Dank, dass Sie bei unserem Experiment mitmachen!

Wir hoffen, es bereitet Ihnen viel Spass!"""

mytext = visual.TextStim(win        = mywindow,
                         text       = begrtext,
                         alignVert  = 'top',
                         alignHoriz = 'left',
                         pos        = (-.8, 0.8),
                         units      = 'norm',
                         wrapWidth  = 1.6)

# Zeichnen des Stimulus in das Fenster.
mytext.draw()
# Zeichnen des Stimulus auf dem Monitor.
mywindow.flip()

# Die Versuchsperson soll Zeit haben, die Nachricht zu lesen
core.wait(5.0)

# Zeige einen leeren Schirm fuer 2 Sekunden.
mywindow.clearBuffer()
mywindow.flip()
core.wait(2)

# Instruktion
instr1 = """Wir werden Ihnen in diesem Experiment
eine Reihe von Begriffen und Bildern zeigen.

Bitte versuchen Sie, sich diese  gut merken,
wir werden Ihre Erinnerung daran testen!"""

# decode ist notwendig, um Umlaute darzustellen.
mytext.setText(instr1.decode('utf8'))
mytext.draw()
mywindow.flip()
core.wait(5.0)

# Neuer Text
instr2 = """Bitte druecken Sie die Leertaste, um fortzufahren!"""
mytext2 = visual.TextStim(win        = mywindow,
                          text       = instr2.decode('utf8'),
                          units      = 'norm',
                          alignVert  = 'top',
                          alignHoriz = 'left',
                          pos        = (-.8, -0.4),
                          wrapWidth  = 1.6,
                          color      = [0,0,0])

nframes = 180 # entspricht 3secs bei 60Hz Monitoren
for i in range(nframes):
    # Update in place um - 1/nframes. Das '*3' wiederholt den Wert dreimal.
    mytext.color  -= [1./nframes] * 3
    # Update in place um + 1/nframes.
    mytext2.color += [1./nframes] * 3
    mytext.draw()
    mytext2.draw()
    mywindow.flip()

response = None
while response != ['space']:
    response = event.waitKeys()
    print(response)

# Wir schliessen das Fenster und beenden Psychopy.
mywindow.close()
core.quit()
