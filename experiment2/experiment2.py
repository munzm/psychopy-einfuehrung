#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ---------------------------------------------------------------------------
# Importieren von Bibliotheken
# ---------------------------------------------------------------------------
from psychopy import core, visual, gui, event
# Randomisieren der Stimuli
from random import shuffle
# Lesen und schreiben von *.csv Dateien
import csv
# Besser formatierter Output
import pprint
# Enthaelt die Instruktionen
import instruktionen
# Zeit/Datum
import datetime

# ---------------------------------------------------------------------------
# Einstellungen
# ---------------------------------------------------------------------------
# Bequeme Umstellung zwischen test_modus und 'echtem' Modus. Ersterer erlaubt,
# durch das Experiment zu eilen, indem Praesentationszeiten verkuerzt werden.

#  Wenn test_modus auf True gesetzt wird, wird nur ein Testlauf gestartet,
# der Stimuli nur sehr kurz praesentiert.
test_modus = True
exp_version = 0.1
nr_vps = 30

optionen = {
    'pres_ts': .1,  # Darbietungszeiten von Stimuli (Sekunden)
    'pres_fs': 10,  # Darbietungszeiten von Stimuli (Frames)
    'pres_ti': .1,  # Darbietungszeiten von Instruktionen (Sekunde
    'pres_fi': 10,  # Darbietungszeiten von Instruktionen (Frames)
    'pres_tf': .1,  # Darbietungszeiten von Fixationskreuzen  (Sekunde
    'pres_ff': 10,  # Darbietungszeiten von Fixationskreuzen  (Frames)
    'myfullscr': False,  # Vollbildmodus ?
    'verbose'  : True,  # Wie viel Output soll Experimentes gedruckt werden?
    'save_data': True}  # Sollen die Daten gespeichert werden?

if test_modus is False:
    optionen['pres_ts'] = 1
    optionen['pres_fs'] = 120
    optionen['pres_ti'] = 1
    optionen['pres_fi'] = 120
    optionen['pres_tf'] = 1
    optionen['pres_ff'] = 120
    optionen['myfullscr'] = True
    optionen['verbose']   = True
    optionen['save_data'] = False

# Verzeichnis, wo der sich die Stimuli befinden.
dir_stimuli = "./stimuli/"
# Verzeichnis, wo der Output hingeschrieben werden soll.
dir_data = "./data/"

# Dateinamen
fname_stimuli = dir_stimuli + "stimuli-experiment2.csv"
fbasename_output = dir_data + "vp_"

# Datenstrukturen fuer das speichern von Resultaten.
# Liste der gezeigten Stimuli
gezeigte_stimuli = []
# Liste mit Daten, die waehrend des Experimentes gesammelt werden.
results = []

info = {'Versuchsleiter':'', 'exp_version': exp_version,
        'Geschlecht': ['m', 'w'], 'Vp_Nr': range(1, nr_vps)}
infoDlg = gui.DlgFromDict(dictionary=info, title='TestExperiment',
                          fixed = ['exp_version'])

if infoDlg.OK:
    print info
else:
    print 'Der Benutzer hat abgebrochen!'

# Funktionsdefinitionen
def setup_window(monitor):
    "Kreiere einen Monitor"
    mywin = visual.Window(monitor = monitor,
                          fullscr = optionen['myfullscr'])
    return mywin

def setup_stimuli(fname_stimuli, window):
    """Bereite die Stimuli vor."""
    # Einlesen der Stimuli.
    with open(fname_stimuli) as f:
        stimuli = [line.strip() for line in f.readlines()]

    # Wir waehlen die ersten 5 Woerter und Bilder fuer die Praesentation
    # (Randomisieren waere der korrekte Weg).
    stimuli_to_present = stimuli[0:5] + stimuli [10:15]

    # Randomisieren der Praesentationsreihenfolge
    shuffle(stimuli_to_present)

    if optionen['verbose']:
        print "*** Stimuli:"
        pprint.pprint(stimuli)

    # Definition der Stimuli
    fixation_cross = visual.TextStim(window, "+", units = "norm", height = .5)
    instr = visual.TextStim(window, "", font = "Inconsolata", wrapWidth = 50,
                            pos = (-.8, 0), alignHoriz = 'left')
    wort = visual.TextStim(window, "Platzhalter", units = "norm", height = .5)
    bild = visual.ImageStim(window)
    skalenanker = ["sicher neu", "wahrscheinlich neu", "eher neu",
                   "wahrscheinlich alt", "eher alt", "sicher alt"]
    rating_scale = visual.RatingScale(window, low = 1, high = 7,
                                      labels = skalenanker,
                                      choices = skalenanker,
                                      singleClick = False,
                                      stretch = 2.5,
                                      minTime = 0,
                                      mouseOnly = False,
                                      textSize = .6,
                                      acceptSize = 4)

    return(stimuli, stimuli_to_present, fixation_cross, instr, wort, bild,
           rating_scale)

def show_blank(win, time):
    """Zeige ein leeres Fenster."""
    win.clearBuffer()
    win.flip()
    core.wait(time)


def show_fixation(win, nframes, stimulus):
    """Zeige ein Fixationskreuz."""
    win.clearBuffer()
    for i in range(nframes):
        stimulus.draw()
        win.flip()

def show_instr(win, text, nframes):
    """Zeige eine Instruktion."""

    text = text.decode('utf8')
    instr.setText(text)

    for j in range(nframes):
        instr.draw()
        win.flip()


def show_stimulus(win, stimulus_name, dir_stimuli, nframes, wort, bild):
    """Zeige einen Stimulus."""
    if optionen['verbose']:
        print("*** Showing Stimuli: " + stimulus_name)

    if stimulus_name.endswith('jpg'):
        fn = dir_stimuli + stimulus_name
        bild.setImage(fn)
        for i in range(nframes):
            bild.draw()
            win.flip()
    else:
        wort.setText(stimulus_name.decode('utf-8'))
        for i in range(nframes):
            wort.draw()
            win.flip()

    return stimulus_name

# Frage die Versuchsperson, ob sie die Stimuli wiedererkennt:
def ask_recognition(win, rating_scale, stimulus_name, bild, wort, dir_stimuli,
                    presented_stimuli):
    """Einschaetzung, ob Stimulus praesentiert wurde."""

    # Entferne gespeicherte Inhalte der Ratingskala aus vorherigen Trials.
    rating_scale.reset()

    # Zeichne Bilder/Woerter
    if "jpg" in stimulus_name:
        bild.setImage(dir_stimuli + stimulus_name)
        todraw = bild
    else:
        wort.setText(stimulus_name.decode('utf-8'))
        todraw = wort

    # Zeichne die Skala, bis eine Antwort gegeben wird.
    while rating_scale.noResponse:
            todraw.draw()
            rating_scale.draw()
            win.flip()

    if optionen['verbose']:
        print '*** Stimuli = ', stimulus_name
        print 'rating = ', rating_scale.getRating()
        print 'Rt = ', rating_scale.getRT()

    # Ist der Stimulus alt oder neu?
    if stimulus_name in stimuli_to_present:
        stimulus_typ = "alt"
    else:
        stimulus_typ = "neu"

    win.clearBuffer()
    win.flip()

    # Daten, die gespeichert werden sollen.
    return([stimulus_name, stimulus_typ, rating_scale.getRating(),
            rating_scale.getRT(), exp_version, datetime.datetime.now()])

def save_results(outfile, data):
    "Speichere Daten"
    myfile = open(outfile, 'wb')
    wr = csv.writer(myfile, quoting=csv.QUOTE_MINIMAL)
    for i in data:
        wr.writerow(i)

    print("Results written to: " + outfile)

# Das Konstrukt 'if __name__ == main' bewirkt, dass der darin enthaltene Code
# nur ausgefuehrt wird, wenn das Skript vom Terminal gestartet wird.
if __name__ == '__main__':
    mywin = setup_window('uniHp')
    (stimuli, stimuli_to_present, fixation_cross, instr, wort, bild,
    rating_scale) = setup_stimuli(fname_stimuli, mywin)

    if optionen['verbose']:
        print "*** randomisierte Stimuli:"
        print  stimuli

    # Wenn es einen Fehler gibt, wird der Block in 'finally' ausgefuehrt.
    # Das Fenster wird so selbst nach einem Fehler geschlossen.
    try:
        # Zeigen der Begruessung
        show_blank(mywin, optionen['pres_ti'])
        show_instr(mywin, instruktionen.begrtext, optionen['pres_fi'])

        # Zeigen der Instruktionen
        show_blank(mywin, optionen['pres_ti'])
        show_instr(mywin, instruktionen.instr1, optionen['pres_fi'])

        # Zeigen der eigentlichen Stimuli
        for stimulus in stimuli_to_present:
            show_fixation(mywin, optionen['pres_ff'], fixation_cross)
            gezeigte_stimuli.append(show_stimulus(mywin, stimulus, dir_stimuli,
                                               optionen['pres_fs'], wort, bild))
            show_blank(mywin, optionen['pres_ti'])

        # Zeigen des Wiedererkennungstests: Die Bilder/Worte muessen anders
        # positioniert werden, um nicht mit der Skala zu interferieren.
        bild.setPos((0, .3))
        bild.setSize(1)
        wort.setPos((0, .3))
        wort.setSize(.5)

        # Stimulirandomisierung
        shuffle(stimuli)

        # Zeigen der Stimuli
        for stimulus in stimuli:
            results.append(ask_recognition(mywin, rating_scale, stimulus, bild,
                                           wort, dir_stimuli,
                                           stimuli_to_present))

        if optionen['verbose']:
            pprint.pprint(results)

        save_results(dir_data + "vp-" + str(info['Vp_Nr']) + '.csv', results)

        show_instr(mywin, instruktionen.verabtext, optionen['pres_fi'])
    finally:
        # Schliessen des Fensters
        mywin.close()
