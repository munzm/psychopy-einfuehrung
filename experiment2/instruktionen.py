# -*- coding: utf-8 -*-
begrtext = """Liebe Versuchsperson,

Vielen Dank, dass Sie bei unserem Experiment
mitmachen!

Wir hoffen, es bereitet Ihnen viel Spass!"""

# Instruktion
instr1 = """Wir werden Ihnen in diesem Experiment
eine Reihe von Begriffen und Bildern zeigen.

Bitte versuchen Sie, sich diese gut zu merken,
wir werden Ihre Erinnerung daran testen! """

# Verabschiedung
verabtext = """Vielen Dank!

Bitte benachrichtigen sie den Versuchsleiter!"""
