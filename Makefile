.Phony: clean

clean:
	rm -f *.alg *.aux *.bbl *.blg *.dvi *.fdb_latexmk *.out *.pdfsync *.pyg *.pyc *.synctex.gz *.toc *.vrb *.xdy *.html *.pdf *.tex *.log
doc:
	rm -rf unterlagen  && mkdir -p ./unterlagen/slides && emacsclient -e '(m-export-all-psychopy-docs)'
